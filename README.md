# 2GB Week

## Prerequisites

1. GCP instance `n2d-highcpu-2 (2 vCPU, 2GB)`
2. GCP instance `n2d-highcpu-4 (4vCPU, 4GB)`
3. Omnibus running on these instances

## Installation

### Install Omnibus

Code snippets to install Omnibus.

#### 2 cores 2 GB 4 GB swap

##### From Instance Template
Created an instance template for this that also configures 4GB of swap, since we weren't able to install GL without swap:

https://console.cloud.google.com/compute/instanceTemplates/details/omnibus-2c2g4s-template?project=group-memory-testbed-c2c979

Create a new VM instance from the template above, then connect via SSH and run:

```bash
sudo apt-get update && \
sudo apt-get install -y curl openssh-server ca-certificates tzdata && \
sudo debconf-set-selections <<< "postfix postfix/mailname string $(hostname -i)" && \
sudo debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'" && \
sudo apt-get install -y postfix && \
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash && \
sudo EXTERNAL_URL=$(hostname -i) apt-get install gitlab-ee
```

##### From Machine Image
A [machine image](https://cloud.google.com/compute/docs/machine-images) is a Compute Engine resource that stores all the configuration, metadata, permissions,
and data from one or more disks required to create a virtual machine (VM) instance.

Created an machine image based on the VM instance from previous step.

Create a new VM instance from the machine image: `omnibus-2c2g4s-machine-image`,
https://console.cloud.google.com/compute/machineImages/details/omnibus-2c2g4s-machine-image?authuser=4&project=group-memory-testbed-c2c979

Once the VM instance is created, it's ready to be used.

### Configure certificates

Configure LetsEncrypt and use `nip.io`.

### Additional dependencies

Additional dependencies that are useful to tinker with Omnibus.

## Tinkering

Different tricks to modify Omnibus installation live without recompile.

### Edit remote Rails sources in Omnibus

1. open `/opt/gitlab/embedded/service/gitlab-rails/path/to/file.rb`
1. make changes, save
1. `sudo gitlab-ctl restart puma`

### Edit sources remotely with VSCode

1. ssh to VM with your `user`
1. `sudo su`
1. `cp -rv ~<user>/.ssh ~root/`
1. Install `Remote-SSH` plugin (by Microsoft)
1. Action: `Remote-SSH: Connect to Host` with `root@<vm_ip>`
1. wait for it to do its setup 
1. Edit and save as usually

### Change Omnibus config

1. `sudo vim /etc/gitlab/gitlab.rb`
1. make changes
1. `sudo gitlab-ctl reconfigure`

### See Omnibus service status

`sudo gitlab-ctl status`

### Update Omnibus to latest version (optionally)

**NOTE:** This requires that the `postgresql` service is running, since during installation a DB dump and upgrade will be performed (`gitlab-ctl start postgresql`).

Pick any version from https://packages.gitlab.com/gitlab/nightly-builds, or the one below:

```
wget https://packages.gitlab.com/gitlab/nightly-builds/packages/ubuntu/xenial/gitlab-ee_13.5.4+rnightly.177078.0e9bf6a9-0_amd64.deb/download.deb
mv download.deb gitlab-ee_13.5.4+rnightly.177078.0e9bf6a9-0_amd64.deb
dpkg -i gitlab-ee_13.5.4+rnightly.177078.0e9bf6a9-0_amd64.deb
```

### Set memory limit on VM

Be or become root.

- `vim /etc/default/grub`
- set `GRUB_CMDLINE_LINUX="mem=XG"` where `X` is the number of GB
- `update-grub`
- `reboot`

## Metrics to keep an eye on 

`node-exporter` will export useful stats from `/proc` and elsewhere.

See https://github.com/prometheus/node_exporter#enabled-by-default

### Page faults / swap

`curl -s localhost:9100/metrics | grep 'vmstat'`

Watch it live:

`watch -n1 egrep '^pgp.+' /proc/vmstat`

### Configure and run GPT

> https://gitlab.com/gitlab-org/quality/performance

1. You can use GitLab Compose Kit
1. Add a new personal access token with API for Admin account
1. Clone and install GPT
1. Create `gcp-4gb.json` with your environment details
1. Create `.env` file with the following content
1. Load `.env` into your environment `source .env`
1. Seed `GPT` with `bundle exec bin/generate-gpt-data --environment gcp-4gb.json`
1. Run `k6` tests with `bundle exec bin/run-k6 --environment gcp-4gb.json --options 60s_200rps.json`

### Clone and install GPT

```bash
git clone https://gitlab.com/gitlab-org/quality/performance.git /gck/performance
cd /gck/performance/
bundle install
```

#### `.env`

```bash
export ACCESS_TOKEN=<access-token>
export LANG=C.UTF-8
export LC_ALL=C.UTF-8
```

#### `gcp-4gb.json` in `k6/config/environments`

```json
{
  "environment": {
    "name": "gcp-4gb",
    "url": "http://35.242.247.229/",
    "user": "root",
    "config": {
      "latency": "30"
    },
    "storage_nodes": ["default"]
  },
  "gpt_data": {
    "root_group": "gpt",
    "large_projects": {
      "group": "large_projects",
      "project": "gitlabhq"
    },
    "many_groups_and_projects": {
      "group": "many_groups_and_projects",
      "subgroups": 5,
      "subgroup_prefix": "gpt-subgroup-",
      "projects": 5,
      "project_prefix": "gpt-project-"
    }
  }
}
```
